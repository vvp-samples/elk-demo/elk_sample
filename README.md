# ELK Sample

It's a part of the project sample demonstrating work with customs logs by ELK stack (Elasticsearch, Logstash, Kibana).<br />
This part is for running dev ELK on your machine.

WARNING: Don't use it in production.

All you need to run it is docker-compose.

1. Call the command:<br />
docker-compose up [-d]<br />
from the root directory of the project to start it.

2. Import file kibana-exports.ndjson into Kibana. 
    - Open url http://localhost:5601/
    - Click on Management
    - Click on Saved Objects
    - Click on the Import button
    - Browse the exported file
    - Import

3. Look at Kibana Dashboard / Main Dashboard

Call:<br />
docker-compose down [-v]<br />
to stop it.